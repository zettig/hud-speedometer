package com.zettig.hud;

import android.app.Application;

import com.zettig.hud.utils.ManagerShared;


public class AndroidApplication extends Application {

    public static AndroidApplication INSTANCE;
    private ManagerShared sharedManager;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
    }

    public ManagerShared getSharedManager() {
        if (sharedManager == null) {
            setSharedManager(new ManagerShared(getApplicationContext()));
        }
        return sharedManager;
    }

    public void setSharedManager(ManagerShared sharedManager) {
        this.sharedManager = sharedManager;
    }
}
