package com.zettig.hud.presenter;

import android.text.TextUtils;

import com.zettig.hud.model.ISpeedModel;
import com.zettig.hud.model.LogisticData;
import com.zettig.hud.model.SpeedModel;
import com.zettig.hud.view.SpeedometerView;

import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;

public class SpeedometerPresenter implements ISpeedometerPresenter {
    private SpeedometerView view;
    private ISpeedModel speedModel;
    private boolean mHudMode = false;
    private Mode mode;

    public SpeedometerPresenter(SpeedometerView view) {
        this.view = view;
        this.speedModel = new SpeedModel();
        String mod = speedModel.getMode();
        if (TextUtils.isEmpty(mod)){
            mod = Mode.ANALOG.toString();
        }
        mode = Mode.valueOf(mod);
    }

    @Override
    public void onCreate() {
        speedModel.getLogisticDate(new LogisticObserver());
        if (this.mode == Mode.ANALOG){
            this.view.setAnalogMode();
        } else {
            this.view.setDigitalMode();
        }
    }

    @Override
    public void onDestroy() {
        speedModel.dispose();
        this.view = null;
    }

    @Override
    public void onHudModeClick() {
        mHudMode = !mHudMode;
        this.view.setHudMode(mHudMode);
    }

    private void updateSpeed(int speed){
        this.view.setSpeed(speed*3600/1000);
    }

    private void updateDistance(float distance) {
        this.view.setDistance(distance / 1000)  ;
    }

    private class LogisticObserver extends DisposableObserver<LogisticData>{
        @Override
        public void onNext(@NonNull LogisticData logisticData) {
            updateSpeed(logisticData.getSpeed());
            updateDistance(logisticData.getDistance());
        }

        @Override
        public void onError(@NonNull Throwable e) {

        }

        @Override
        public void onComplete() {

        }
    }

    @Override
    public void onModeClick() {
        if (mode == Mode.ANALOG){
            mode = Mode.DIGITAL;
            this.view.setDigitalMode();
            speedModel.setMode(mode.toString());
        } else {
            mode = Mode.ANALOG;
            this.view.setAnalogMode();
            speedModel.setMode(mode.toString());
        }
    }

    private enum Mode {
        ANALOG, DIGITAL
    }
}
