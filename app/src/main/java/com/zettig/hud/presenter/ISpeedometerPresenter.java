package com.zettig.hud.presenter;



public interface ISpeedometerPresenter {
    void onCreate();
    void onDestroy();
    void onHudModeClick();
    void onModeClick();
}
