package com.zettig.hud.view;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.zettig.hud.R;
import com.zettig.hud.presenter.ISpeedometerPresenter;
import com.zettig.hud.presenter.SpeedometerPresenter;
import com.zettig.hud.view.CustomView.Speedometer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements SpeedometerView {

    @BindView(R.id.speedometer) Speedometer speedometer;
    @BindView(R.id.btnSpeedometerMode) ImageView btnMode;
    @BindView(R.id.main_root_layout) View rootLayout;

    private static final int REQUEST_LOCATION_PERMISSION_CODE = 1234;
    private static final int PERMISSION_REQUEST_CODE = 4321;

    private ISpeedometerPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        presenter = new SpeedometerPresenter(this);
        checkPermissions();

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void initSpeedometer(){
        presenter.onCreate();
    }


    public void openApplicationSettings() {
        Intent appSettingsIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.parse("package:" + getPackageName()));
        startActivityForResult(appSettingsIntent, PERMISSION_REQUEST_CODE);
        Toast.makeText(this, R.string.grant_location_permission, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PERMISSION_REQUEST_CODE) {
            checkPermissions();
        }
    }


    private void checkPermissions(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED ||
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED){
            requestLocationPermissions();
        } else {
            initSpeedometer();
        }
    }

    public void requestLocationPermissions(){
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
        ActivityCompat.requestPermissions(this, permissions, REQUEST_LOCATION_PERMISSION_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOCATION_PERMISSION_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED){
            initSpeedometer();
        } else {
            showNeedPermissionsSnackBar();
        }
    }

    void showNeedPermissionsSnackBar(){
        Snackbar.make(rootLayout, R.string.location_permission_need, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.grant, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showRequestPermissions();
                    }
                })
                .show();
    }

    private void showRequestPermissions(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)){
            requestLocationPermissions();
        } else {
            openApplicationSettings();
        }
    }


    @Override
    public void setSpeed(int speed) {
        Log.d("TAG", "setSpeed: " + speed);
        speedometer.setSpeed(speed);
    }

    @Override
    public void setHudMode(boolean enabled) {
        if (enabled){
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
        speedometer.setHudModeEnabled(enabled);
    }

    @Override
    public void setDistance(float distance) {
        speedometer.setDistance(distance);
    }

    @OnClick(R.id.btnHudMode)
    public void onBtnHudModeClick(){
        presenter.onHudModeClick();
    }

    @OnClick(R.id.btnSpeedometerMode)
    public void onSpeedometerModeClick(){
        presenter.onModeClick();
    }

    @Override
    public void setAnalogMode() {
        speedometer.setMode(Speedometer.MODE_ANALOG);
        btnMode.setImageResource(R.drawable.ic_speedometer_digital);
    }

    @Override
    public void setDigitalMode() {
        speedometer.setMode(Speedometer.MODE_DIGITAL);
        btnMode.setImageResource(R.drawable.ic_speedometer_analog);
    }
}
