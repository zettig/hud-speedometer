package com.zettig.hud.view.CustomView;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;

import com.zettig.hud.R;


public class Speedometer extends View {

    public final static int MODE_ANALOG = 100;
    public final static int MODE_DIGITAL = 200;

    private int mMaxSpeed;
    private boolean mEnableHudMode;
    private int mSpeed;
    private int mMode;
    private float mDistance;
    private int mColorResId;
    private int mPointerColorResId;
    private int mBgColorResId;

    private int padding = 30;
    private Paint paint;
    private TextPaint textPaint;
    private final RectF conturRect = new RectF();
    private final RectF scaleRect = new RectF();
    float radius;

    public void setSpeed(int speed) {
        if (speed < 0){
            throw new IllegalArgumentException("Non-positive value specified as a speed.");
        }
        if (speed > mMaxSpeed){
            speed = mMaxSpeed;
        }

        ValueAnimator valueAnimator = ValueAnimator.ofInt(this.mSpeed, speed);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                updateSpeed((Integer) valueAnimator.getAnimatedValue());
            }
        });
        valueAnimator.start();

    }

    public void setMode(int mode) {
        this.mMode = mode;
        invalidate();
    }

    public void setHudModeEnabled(boolean enableHudMode) {
        this.mEnableHudMode = enableHudMode;
        invalidate();
    }

    private void updateSpeed(int speed){
        this.mSpeed = speed;
        invalidate();
    }

    public void setDistance(float distance) {
        this.mDistance = distance;
        invalidate();
    }

    public Speedometer(Context context) {
        super(context);
    }

    public Speedometer(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.Speedometer);
        this.mMaxSpeed = typedArray.getInt(R.styleable.Speedometer_spd_maxSpeed, 200);
        this.mEnableHudMode = typedArray.getBoolean(R.styleable.Speedometer_spd_enableHudMode, false);
        this.mMode = typedArray.getInt(R.styleable.Speedometer_spd_mode, MODE_ANALOG);
        this.mColorResId = typedArray.getInt(R.styleable.Speedometer_spd_color, Color.parseColor("#42f4f1"));
        this.mPointerColorResId = typedArray.getInt(R.styleable.Speedometer_spd_pointer_color, Color.RED);
        this.mBgColorResId = typedArray.getInt(R.styleable.Speedometer_spd_background_color, Color.BLACK);

        typedArray.recycle();
        paint = new Paint();
        textPaint = new TextPaint();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor(mBgColorResId);
        float center_x = getWidth()/2;
        float center_y = getHeight()/2;
        radius = Math.min(center_x, center_y) -padding;
        conturRect.set(center_x - radius,
                center_y - radius,
                center_x + radius,
                center_y + radius);

        scaleRect.set(center_x - radius - 48 ,
                center_y - radius - 48 ,
                center_x + radius + 48,
                center_y + radius + 48);

        if (mEnableHudMode){
            canvas.scale(1, -1, getWidth() / 2, getHeight() / 2);
        }
        switch (mMode){
            case MODE_ANALOG:
                textPaint.setTypeface(Typeface.DEFAULT);
                textPaint.setTextSize(Math.min(getWidth(), getHeight())/15);
                drawAnalogSpeedometer(canvas);
                break;
            case MODE_DIGITAL:
                textPaint.setTypeface(Typeface.createFromAsset(getContext().getAssets(),"fonts/a_lcdnova.ttf"));
                textPaint.setTextAlign(Paint.Align.CENTER);
                textPaint.setTextSize(Math.min(getWidth(), getHeight())/5);
                textPaint.setColor(Color.WHITE);
                drawDigitalSpeedometer(canvas);
                break;
        }
    }

    private void drawAnalogSpeedometer(Canvas canvas){
        drawAnalogDial(canvas);
        drawAnalogPointer(canvas);
        drawAnalogOdometer(canvas);
    }

    private void drawDigitalSpeedometer(Canvas canvas){
        paint.setColor(mColorResId);
        paint.setStrokeWidth(convertDpToPixel(10));
        paint.setStyle(Paint.Style.STROKE);
        for (int i = 0; i <= mMaxSpeed; i = i + 4){
            float angle = (float) i /mMaxSpeed * 260 - 40;
            int length = (int)convertDpToPixel(20);
            if (i > mMaxSpeed / 3 * 2){
                paint.setColor(Color.RED);
            } else {
                paint.setColor(mColorResId);
            }
            if (i > mSpeed - 4){
                paint.setAlpha(70);
            } else {
                paint.setAlpha(255);
            }
            canvas.drawLine(
                    (float) (scaleRect.centerX() + Math.cos((180 - angle) / 180 * Math.PI) * (radius + convertDpToPixel(5))),
                    (float) (scaleRect.centerY() - Math.sin(angle / 180  * Math.PI) * (radius + convertDpToPixel(5))),
                    (float) (scaleRect.centerX() + Math.cos((180 - angle) / 180 * Math.PI) * (radius - length)),
                    (float) (scaleRect.centerY() - Math.sin(angle / 180  * Math.PI) * (radius - length)), paint);
        }
        canvas.drawText(String.valueOf(mSpeed), getWidth()/2, getHeight()/2, textPaint);
        textPaint.setTextSize(Math.min(getWidth(), getHeight())/15);
        canvas.drawText(getResources().getString(R.string.km_h), getWidth()/2, getHeight()/10*6, textPaint);
        textPaint.setTextSize(Math.min(getWidth(), getHeight())/10);
        canvas.drawText(String.format(getResources().getString(R.string.odometer_text), mDistance), getWidth()/2, conturRect.bottom, textPaint);
    }


    private void drawAnalogDial(Canvas canvas){


        //внешний овал
        paint.setColor(mColorResId);
        paint.setStrokeWidth(convertDpToPixel(10));
        paint.setStyle(Paint.Style.STROKE);

        int startAngle = 140;
        int sweepAngle = 260;
        canvas.drawArc(conturRect, startAngle, sweepAngle, false, paint);


        //шкала
        textPaint.setColor(Color.WHITE);
        textPaint.setTextAlign(Paint.Align.CENTER);
        paint.setStrokeWidth(convertDpToPixel(5));
        for(int i = 0; i <= mMaxSpeed; i = i + 10){
            float angle = (float) i/ mMaxSpeed * 260 - 40;
            float length;
            if (i % 20 == 0){
                paint.setAlpha(255);
                length = convertDpToPixel(20);
                //циферблат
                canvas.drawText(String.valueOf(i),(float) (scaleRect.centerX() + Math.cos((180 - angle) / 180 * Math.PI) * (radius - convertDpToPixel(45))),
                        (float) (scaleRect.centerY() - Math.sin(angle / 180  * Math.PI) * (radius - convertDpToPixel(45))), textPaint);
            } else {
                paint.setAlpha(100);
                length = convertDpToPixel(20);
            }
            canvas.drawLine(
                    (float) (scaleRect.centerX() + Math.cos((180 - angle) / 180 * Math.PI) * (radius + convertDpToPixel(5))),
                    (float) (scaleRect.centerY() - Math.sin(angle / 180  * Math.PI) * (radius + convertDpToPixel(5))),
                    (float) (scaleRect.centerX() + Math.cos((180 - angle) / 180 * Math.PI) * (radius - length)),
                    (float) (scaleRect.centerY() - Math.sin(angle / 180  * Math.PI) * (radius - length)), paint);
        }
        textPaint.setTextSize(convertSpToPixels(16));
        textPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.ITALIC));
        canvas.drawText(getResources().getString(R.string.km_h),conturRect.centerX(), conturRect.centerY() - conturRect.height()/5, textPaint);
        textPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.NORMAL));
    }



    private void drawAnalogPointer(Canvas canvas){
        paint.setColor(mPointerColorResId);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setStrokeWidth(convertDpToPixel(7));
        canvas.drawCircle(conturRect.centerX(), conturRect.centerY(), convertDpToPixel(5), paint);
        float radius = Math.min(conturRect.centerX(), conturRect.centerY()) - padding - convertDpToPixel(30);
        float angle = (float) mSpeed / mMaxSpeed * 260 - 40;

        canvas.drawLine(conturRect.centerX(), conturRect.centerY(), (float) (conturRect.centerX() + Math.cos((180 - angle) / 180 * Math.PI) * (radius)),
                (float) (conturRect.centerY() - Math.sin(angle / 180  * Math.PI) * (radius)), paint);
    }


    private void drawAnalogOdometer(Canvas canvas){
        textPaint.setTextSize(convertSpToPixels(18));
        canvas.drawText(String.format(getResources().getString(R.string.odometer_text), mDistance), conturRect.centerX(), conturRect.centerY() + conturRect.height()/4, textPaint);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int desiredWidth = 200;
        int desiredHeight = 200;

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width;
        int height;

        if (widthMode == MeasureSpec.EXACTLY) {
            width = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            width = Math.min(desiredWidth, widthSize);
        } else {
            width = desiredWidth;
        }

        if (heightMode == MeasureSpec.EXACTLY) {
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            height = Math.min(desiredHeight, heightSize);
        } else {
            height = desiredHeight;
        }

        setMeasuredDimension(width, height);
    }

    // dp to px
    public float convertDpToPixel(float dp){
        Resources resources = getContext().getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    // sp to px
    public int convertSpToPixels(float sp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, getContext().getResources().getDisplayMetrics());
    }
}
