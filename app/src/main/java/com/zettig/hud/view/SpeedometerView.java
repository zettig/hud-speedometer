package com.zettig.hud.view;


public interface SpeedometerView {
    void setSpeed(int speed);
    void setHudMode(boolean enabled);
    void setDistance(float distance);
    void setAnalogMode();
    void setDigitalMode();
}
