package com.zettig.hud.model;

import io.reactivex.observers.DisposableObserver;

public interface ISpeedModel {
    void getLogisticDate(DisposableObserver<LogisticData> observer);
    void dispose();
    String getMode();
    void setMode(String mode);
}
