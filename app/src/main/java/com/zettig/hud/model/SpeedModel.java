package com.zettig.hud.model;

import android.location.Location;

import com.google.android.gms.location.LocationRequest;
import com.patloew.rxlocation.RxLocation;
import com.zettig.hud.AndroidApplication;
import com.zettig.hud.utils.SharedKeys;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;

public class SpeedModel implements ISpeedModel {
    private Location mTmpLocation;
    private float mDistance = 0;
    private RxLocation rxLocation;
    private final CompositeDisposable disposables;

    public SpeedModel() {
        this.disposables = new CompositeDisposable();
        rxLocation = new RxLocation(AndroidApplication.INSTANCE.getApplicationContext());
    }

    @Override
    public void getLogisticDate(DisposableObserver<LogisticData> observer) {

        LocationRequest locationRequest = LocationRequest.create()
                .setInterval(0)
                .setSmallestDisplacement(0)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        addDisposable(rxLocation.location().updates(locationRequest)
                .flatMap(new Function<Location, ObservableSource<LogisticData>>() {
                    @Override
                    public ObservableSource<LogisticData> apply(@NonNull Location location) throws Exception {
                        if (mTmpLocation != null && mTmpLocation.getLatitude() != location.getLatitude() && mTmpLocation.getLongitude() != location.getLongitude()) {
                            mDistance += location.distanceTo(mTmpLocation);
                        }
                        mTmpLocation = location;
                        LogisticData logisticData = new LogisticData((int) location.getSpeed(), mDistance);
                        return Observable.just(logisticData);
                    }
                }).subscribeWith(observer));
    }

    @Override
    public void dispose() {
        if (!disposables.isDisposed()) {
            disposables.dispose();
        }
    }

    private void addDisposable(Disposable disposable) {
        if (disposable == null || disposables == null){
            throw  new NullPointerException();
        }
        disposables.add(disposable);
    }

    @Override
    public String getMode() {
        return AndroidApplication.INSTANCE.getSharedManager().getValueString(SharedKeys.SPEEDOMETER_MODE_KEY);
    }

    @Override
    public void setMode(String mode) {
        AndroidApplication.INSTANCE.getSharedManager().putKeyString(SharedKeys.SPEEDOMETER_MODE_KEY, mode);
    }
}
