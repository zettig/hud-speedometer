package com.zettig.hud.model;


public class LogisticData {
    private int speed;
    private float distance;

    LogisticData(int speed, float distance) {
        this.speed = speed;
        this.distance = distance;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }
}
